//
//  HomeCard.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 6/15/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit

class HomeCard: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var textView: UIView!
    
    


    override init(frame: CGRect) {
        //for using CustomView in code
        
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        //We're going to do stuff here
        
        Bundle.main.loadNibNamed("HomeCard", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
       
        
        
    }
    
    public func setCardImage(image img: UIImage){
        imageView.image = img
    }
    
    
    
  
    
}
