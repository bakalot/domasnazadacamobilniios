//
//  PromoTableViewCell.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 12/17/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellDescription: UILabel!
    
    
    func setPromoCell(title: String, description: String){
        
//        self.cellImageView.image = image
        self.cellTitle.text = title
        self.cellDescription.text = description
    }

}
