//
//  ShoppingCartTableViewCell.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 1/23/19.
//  Copyright © 2019 Martin Stojcev. All rights reserved.
//

import UIKit

class ShoppingCartTableViewCell: UITableViewCell {

    @IBOutlet weak var shoppingImageView: UIImageView!
    
    @IBOutlet weak var shoppingItemTitle: UILabel!
    @IBOutlet weak var shoppingItemQuantity: UILabel!
    @IBOutlet weak var shoppingTotalPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setShoppingCell(image: UIImage, title: String, quantity: Int, totalPrice: Float) {
        self.shoppingImageView.image = image
        self.shoppingItemTitle.text = title
        self.shoppingItemQuantity.text = String(quantity)
        self.shoppingTotalPrice.text = String(totalPrice) + " DEN"
    }

}
