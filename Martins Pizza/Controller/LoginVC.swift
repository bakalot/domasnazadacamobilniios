//
//  LoginVC.swift
//  Martin's Pizza
//
//  Created by Martin Stojcev on 6/16/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit
import TKSubmitTransition
import DTTextField
import Firebase

class LoginVC: UIViewController, UIViewControllerTransitioningDelegate {

    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var logoImgView: UIImageView!
    @IBOutlet weak var headerTextLabel: UILabel!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var forgotBtn: UIButton!
    @IBOutlet weak var loginBtn: TKTransitionSubmitButton!
    @IBOutlet weak var registerBtn: TKTransitionSubmitButton!
    
    @IBOutlet weak var emailTxtField: DTTextField!
    @IBOutlet weak var passwordTxtField: DTTextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view.backgroundColor = AppColor.black.rawValue
        navigationBar.tintColor = UIColor.white
        navigationBar.barTintColor = AppColor.black.rawValue
        backBtn.title = ""
        backBtn.setBackgroundImage(UIImage(named: "backButtonImg"), for: .normal, barMetrics: UIBarMetrics.default)
        
        headerView.backgroundColor = AppColor.black.rawValue
        
        logoImgView.image = UIImage(named: "headerLogo")
        
        headerTextLabel.textColor = AppColor.red.rawValue
        titleLabel.textColor = UIColor.white
        
        loginBtn.backgroundColor = AppColor.green.rawValue
        loginBtn.tintColor = UIColor.white
        loginBtn.layer.cornerRadius = 5
        
        registerBtn.backgroundColor = AppColor.blue.rawValue
        registerBtn.tintColor = UIColor.white
        registerBtn.layer.cornerRadius = 5
        
        forgotBtn.tintColor = UIColor.white
        
       // showToolbar()
        
        
    }
    
    @IBAction func loginUser(_ sender: Any) {
        
        
        
        if let emailLogin = emailTxtField.text {
            
            if (emailLogin == ""){
                // insert email
                emailTxtField.showError(message: "Email must be inserted")
            }
            else {
                //check for email format
                
                let validLoginEmail = isValidEmail(email: emailLogin)
                
                if (validLoginEmail){
                    //check for password
                    
                    
                    if let password = passwordTxtField.text {
                        
                        if password == "" {
                            //password is not inserted
                            passwordTxtField.showError(message: "Password must be inserted")
                        }
                        else {
                             //attempt to login the user
                            loginBtn.startLoadingAnimation()
                            
                            Auth.auth().signIn(withEmail: emailLogin, password: password) { (succ, error) in
                                
                                if let err = error {
                                    // error in sign in
                                    print("Can not sign in")
                                    print(err)
                                    
                                    self.loginBtn.startFinishAnimation(0.2, completion: {
                                        
                                        print("CANNOT SIGN IN")
                                    })
                                }
                                
                                else if let success = succ{
                                    //successfully signed in
                                    
                                    self.loginBtn.startFinishAnimation(0.5, completion: {
                                        
                                        // show home vc
                                        
                                        if let homeVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeVC") as? HomeVC {
                                            
                                            homeVC.transitioningDelegate = self
                                            self.present(homeVC, animated: true)
                                            
                                        }
                                        
                                        
                                    })
                                    
                                }
                                
                                
                            }
                            
                        }
                        
                        
                        
                    }
                    else {
                        
                        print("error password")
                        
                    }
                    
                    
                    
                    
                    
                }
                else {
                    //email format isn't correct
                    emailTxtField.showError(message: "Email isn't in the desired format")
                }
                
                
            }
            
        }
        else {
            print("Error email")
        }
        
    }
    
    
    func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    
    @IBAction func registerBtnAction(_ sender: Any) {
        didStartYourLoading()
        
    }
    
    func didStartYourLoading(){
       registerBtn.startLoadingAnimation()
        didFinishYourLoading()
       
    }
    
    func didFinishYourLoading(){
        
        registerBtn.startFinishAnimation(1) {
           
            if let registerVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegisterVC") as? RegisterVC {
                
                registerVC.transitioningDelegate = self
                self.present(registerVC, animated: true)
                
            }
            
        }
    }
    
//    func showToolbar(){
//        
//        let toolbarKeyboard = UIToolbar()
//        toolbarKeyboard.sizeToFit()
//        
//        let spaceBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
//        let cancelBtn = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelToolbar))
//        
//        toolbarKeyboard.setItems([spaceBtn,cancelBtn], animated: false)
//        
//        emailTxtField.inputAccessoryView = toolbarKeyboard
//        passwordTxtField.inputAccessoryView = toolbarKeyboard
//        
//        
//        
//    }
    
    @objc func cancelToolbar(){
        
        self.view.endEditing(true)
        //scrollView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TKFadeInAnimator(transitionDuration: 0.5, startingAlpha: 0.8)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
