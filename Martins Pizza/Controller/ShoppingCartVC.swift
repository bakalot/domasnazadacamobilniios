//
//  ShoppingCartVC.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 1/7/19.
//  Copyright © 2019 Martin Stojcev. All rights reserved.
//

import UIKit

class ShoppingCartVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    @IBOutlet weak var placeOrderButton: UIButton!
    @IBOutlet weak var shoppingTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let totalPrice = (CartManager.sharedInstance.totalPriceInCart() * 50 ).rounded()
        totalPriceLabel.text = "Total price: " + String(totalPrice) + " DEN"
    }
    

    @IBAction func placeOrder(_ sender: Any) {
        let alert = UIAlertController(title: "Order placed", message: "Your orders successfully placed. Payment will be processed when you receive your order.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            CartManager.sharedInstance.clearCart()
            self.totalPriceLabel.text = ""
            self.shoppingTableView.reloadData()
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CartManager.sharedInstance.cartItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "shoppingCell")as? ShoppingCartTableViewCell else{return UITableViewCell() }
         let currentItem = CartManager.sharedInstance.productAtIndexPath(indexPath: indexPath)
         let title = currentItem.title ?? ""
         let image = currentItem.image ?? UIImage()
         let price = currentItem.price ?? 0.0
         let quantity = currentItem.quantity ?? 0
         let totalPrice = (price * Float(quantity) * 50.0 ).rounded()
         cell.setShoppingCell(image: image, title: title, quantity: quantity, totalPrice: totalPrice)
        return cell
    }
    
    @IBAction func clearShoppingCart(_ sender: Any) {
        CartManager.sharedInstance.clearCart()
        self.totalPriceLabel.text = ""
        shoppingTableView.reloadData()
    }
    
}
