//
//  DeliveryVC.swift
//  Martin's Pizza
//
//  Created by Martin Stojcev on 6/16/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit
import MapKit

class DeliveryVC: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var deliveryMapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        putPinsOnMap()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func putPinsOnMap(){
        
        let martinsPizzaKarpos = MKPointAnnotation()
        martinsPizzaKarpos.title = "Martin's Pizza"
        martinsPizzaKarpos.coordinate = CLLocationCoordinate2D(latitude: 42.004155, longitude: 21.406914)
        deliveryMapView.addAnnotation(martinsPizzaKarpos)
        
        let martinsPizzaAerodrom = MKPointAnnotation()
        martinsPizzaAerodrom.title = "Martin's Pizza"
        martinsPizzaAerodrom.coordinate = CLLocationCoordinate2D(latitude: 41.985406, longitude: 21.464896)
        deliveryMapView.addAnnotation(martinsPizzaAerodrom)
        
        let martinsPizzaGjorce = MKPointAnnotation()
        martinsPizzaGjorce.title = "Martin's Pizza"
        martinsPizzaGjorce.coordinate = CLLocationCoordinate2D(latitude: 42.010855, longitude: 21.344593)
        deliveryMapView.addAnnotation(martinsPizzaGjorce)
        
        
        let martinsPizzaVeles = MKPointAnnotation()
        martinsPizzaVeles.title = "Martin's Pizza"
        martinsPizzaVeles.coordinate = CLLocationCoordinate2D(latitude: 41.716992, longitude: 21.773563)
        deliveryMapView.addAnnotation(martinsPizzaVeles)
        
        let martinsPizzaKavadarci = MKPointAnnotation()
        martinsPizzaKavadarci.title = "Martin's Pizza"
        martinsPizzaKavadarci.coordinate = CLLocationCoordinate2D(latitude: 41.434280, longitude: 22.010642)
        deliveryMapView.addAnnotation(martinsPizzaKavadarci)
        
        let martinsPizzaGevgelija = MKPointAnnotation()
        martinsPizzaGevgelija.title = "Martin's Pizza"
        martinsPizzaGevgelija.coordinate = CLLocationCoordinate2D(latitude: 41.143641, longitude: 22.503245)
        deliveryMapView.addAnnotation(martinsPizzaGevgelija)
        
        
        
        
        
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }

}
