//
//  AccountVC.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 7/1/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit
import DTTextField
import Firebase
import FirebaseAuth
import SwiftEntryKit


class AccountVC: UIViewController {
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var backBarItem: UIBarButtonItem!
    @IBOutlet weak var saveBarItem: UIBarButtonItem!
    @IBOutlet weak var navigationTitle: UINavigationItem!
    
    @IBOutlet weak var selectStackView: UIStackView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var loginLbl: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var separatorLbl: UILabel!
    @IBOutlet weak var personalInfoView: UIView!
    @IBOutlet weak var personalInfoLbl: UILabel!
    
    @IBOutlet weak var loginContentView: UIView!
    @IBOutlet weak var personalContentView: UIView!
    
    //LoginView
    @IBOutlet weak var emailTxtField: DTTextField!
    @IBOutlet weak var oldPasswordTxtField: DTTextField!
    @IBOutlet weak var newPasswordTxtField: DTTextField!
    @IBOutlet weak var confirmPasswordTxtField: DTTextField!
    
    //PersonalView
    @IBOutlet weak var firstNameTxtField: DTTextField!
    @IBOutlet weak var lastNameTxtField: DTTextField!
    @IBOutlet weak var dateOfBirth: UITextField!
    @IBOutlet weak var mobileNumberTxtField: DTTextField!
    
    // documentID for each user is different
    var databaseDocumentID:String = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")

        view.backgroundColor = AppColor.black.rawValue
        
        navigationBar.barTintColor = AppColor.black.rawValue
        navigationBar.tintColor = UIColor.white
        navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        navigationTitle.title = "Account"
        
    
        
        
        backBarItem.title = ""
        backBarItem.setBackgroundImage(UIImage(named: "backButtonImg"), for: .normal, barMetrics: UIBarMetrics.default)
        saveBarItem.title = "Save"
        saveBarItem.tintColor = AppColor.red.rawValue
        
        
        setupChooseView()
        personalContentView.backgroundColor = AppColor.black.rawValue
        loginContentView.backgroundColor    = AppColor.black.rawValue
        
        //Adding tap gesture to the choosing views
        let tappedLoginView = UITapGestureRecognizer(target: self, action: #selector(showLoginContentView))
        loginView.addGestureRecognizer(tappedLoginView)

        let tappedPersonalInfoView = UITapGestureRecognizer(target: self, action: #selector(showPersonalInfoView))
        personalInfoView.addGestureRecognizer(tappedPersonalInfoView)
        
        personalContentView.isHidden = true
        personalInfoLbl.textColor = UIColor.gray
        
        if let currentUsersEmail = Auth.auth().currentUser?.email {
             emailTxtField.placeholder = currentUsersEmail
        
        }
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupChooseView(){
        
        loginView.backgroundColor        = AppColor.black.rawValue
        loginLbl.textColor               = UIColor.white
        separatorView.backgroundColor    = AppColor.black.rawValue
        separatorLbl.textColor           = UIColor.white
        personalInfoView.backgroundColor = AppColor.black.rawValue
        personalInfoLbl.textColor        = UIColor.white
        
    }
    
    
    @objc func showLoginContentView(){
        
        print("tapped login view")
        personalContentView.isHidden = false
        loginContentView.isHidden    = true
        personalInfoLbl.textColor    = UIColor.gray
        loginLbl.textColor           = UIColor.white
        
    }
    
    @objc func showPersonalInfoView(){
        
        print("tapped personal info view")
        personalContentView.isHidden   = true
        loginContentView.isHidden      = false
        personalInfoLbl.textColor    = UIColor.white
        loginLbl.textColor           = UIColor.gray
        setupPersonalView()
        
    }


    func showPasswordChangedAlert(title title:String, description description:String, color color: UIColor, imageName imgName: String){
    
    var attributes = EKAttributes()
    
    let widthConstraint = EKAttributes.PositionConstraints.Edge.ratio(value: 0.9)
    let heightConstraint = EKAttributes.PositionConstraints.Edge.intrinsic
    let entryColor = EKAttributes.BackgroundStyle.color(color: color)
    let screenBlur = EKAttributes.BackgroundStyle.visualEffect(style: UIBlurEffectStyle.light)
    
    attributes.border = .value(color: AppColor.black.rawValue, width: 1)
    attributes.roundCorners = .all(radius: 20)
    attributes.displayDuration = .infinity
    attributes.screenInteraction = .dismiss
    attributes.positionConstraints.verticalOffset = 0
    attributes.positionConstraints.size = .init(width: widthConstraint, height: heightConstraint)
    attributes.entryBackground = entryColor
    attributes.screenBackground = screenBlur
    attributes.position = .center
    attributes.windowLevel = .alerts
    

    let title = EKProperty.LabelContent(text: title, style: .init(font: UIFont.boldSystemFont(ofSize: 18), color: AppColor.black.rawValue, alignment: NSTextAlignment.center, numberOfLines: 1))
    
    let description = EKProperty.LabelContent(text: description, style: .init(font: UIFont.boldSystemFont(ofSize: 14), color: UIColor.white, alignment: NSTextAlignment.center, numberOfLines: 0))
    let image = EKProperty.ImageContent(image: UIImage(named: imgName)!)
    

    let btnTitle = EKProperty.LabelContent(text: "OK", style: .init(font: .boldSystemFont(ofSize: 20), color: color))
    
       
        
        let btnContent = EKProperty.ButtonContent(label: btnTitle, backgroundColor: AppColor.black.rawValue, highlightedBackgroundColor: UIColor.white) {
            
            if color == AppColor.green.rawValue {
                self.oldPasswordTxtField.text     = ""
                self.newPasswordTxtField.text     = ""
                self.confirmPasswordTxtField.text = ""
            }
            
            SwiftEntryKit.dismiss()
        }
    
    let btnBarContent = EKProperty.ButtonBarContent(with: btnContent, separatorColor: AppColor.black.rawValue, buttonHeight: 25, expandAnimatedly: true)

    let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
    
    let imgPosition = EKAlertMessage.ImagePosition.top
    
    let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, imagePosition: imgPosition, buttonBarContent: btnBarContent)
    

    let alertMessageView = EKAlertMessageView(with: alertMessage)
    SwiftEntryKit.display(entry: alertMessageView, using: attributes)

    }
    
    @IBAction func updateInformations(_ sender: Any) {
        
        //
        
        let isLogin = isLoginView()
        print("isLogin: \(isLogin)")
        
        
        if isLogin {

        // Prompt the user to re-provide their sign-in credentials
        
        //Check for logged in user
        
        let usersEmail = Auth.auth().currentUser?.email ?? nil
        
        let usersOldPassword     = oldPasswordTxtField.text
        let usersNewPassword     = newPasswordTxtField.text
        let usersConfirmPassword = confirmPasswordTxtField.text
        
       
    
        
        if usersOldPassword != "" {
             //check for new and confirm password
            
            let checkPasses = checkPasswordsEquals(newPassword: usersNewPassword!, confirmPassword: usersConfirmPassword!)
            
            if checkPasses {
            print("password are the same, continue to change the password!")
                //try to reauthenticate the user
                
                let emailAuth = EmailAuthProvider.credential(withEmail: usersEmail!, password: usersOldPassword!)
                
                Auth.auth().currentUser?.reauthenticate(with: emailAuth, completion: { (error) in
                    
                    if error != nil {
                        //print(error?.localizedDescription)
                        self.showPasswordChangedAlert(title: "Password error!",
                                                      description: (error?.localizedDescription)!,
                                                      color: AppColor.red.rawValue,
                                                      imageName: "errorImg")
                        self.oldPasswordTxtField.showError(message: "Wrong password!")
                    }
                    else {
                        //successfully reathenticated, now change the password
                        
                        let user = Auth.auth().currentUser;
                        
                        user?.updatePassword(to: usersNewPassword!, completion: { (error) in
                            
                            if error != nil {
                                print("error while changing the password!")
                                self.showPasswordChangedAlert(title: "Password error!",
                                                         description: (error?.localizedDescription)!,
                                                         color: AppColor.red.rawValue,
                                                         imageName: "errorImg")
                            }
                            else {
                                //successfully changed the password
                                print("successfully changed the password")
                                self.showPasswordChangedAlert(title: "Password changed!",
                                                              description: "Your password has been changed.",
                                                              color: AppColor.green.rawValue,
                                                              imageName: "updateInfo"
                                                             )
                            }
                            
                            
                        })
                        
                        
                        
                        
                    }
                    
                })
                
            
            }
            else {
                //new pass and confirm pass are not equal
                print(" New password and confirm password are not equal")
            }
        }
        else {
            // password is not inserted
            oldPasswordTxtField.showError(message: "Password must be filled!")
        }
        

        //
        
    }
    
        else {
            // manage info for the person view
            
            
            if databaseDocumentID != "" {
            
                if let firstNameUpdated = firstNameTxtField.text {
                    
                    
                   updatePersonsName(firstName: firstNameUpdated)
                    
                }
                
                if let lastNameUpdated = lastNameTxtField.text {
                    
                    
                    updatePersonsLastName(lastName: lastNameUpdated)
                    
                }
                
                if let numberUpdated = mobileNumberTxtField.text {
                    
                    
                    updatePersonsMobile(number: numberUpdated)
                    
                }
                
                
            }
            
           
            
            
        }
       
        
    }
    
    
    func isLoginView() -> Bool{
        
        if loginContentView.isHidden == false{
            return true
        }
        else {
            return false
        }
        
        
    }
    
    func updatePersonsMobile(number: String) {
        
        let db = Firestore.firestore()
        
        let numberRef = db.collection("users").document(databaseDocumentID)
        
        
        
        numberRef.updateData(
        ["mobileNumber" : number]) { (error) in
            
            if error != nil {
                print("Error updating the user's mobile number!")
                
            }
            else{
                print("Mobile number updated successfully")
                
            }
            
        }
        
    }
    
    func updatePersonsName(firstName name:String) {
        
        let db = Firestore.firestore()
        
        let nameRef = db.collection("users").document(databaseDocumentID)
        
        
        
        nameRef.updateData(
        ["personsName" : name]) { (error) in
            
            if error != nil {
                print("Error updating the user's first name!")
                
            }
            else{
                print("First name updated successfully")
                
            }
   
        }
        
    }
    
    func updatePersonsLastName(lastName: String) {
        
        let db = Firestore.firestore()
        
        let lastnameRef = db.collection("users").document(databaseDocumentID)
        
        
        
        lastnameRef.updateData(
        ["personsLastName" : lastName]) { (error) in
            
            if error != nil {
                print("Error updating the user's last` name!")
                
            }
            else{
                print("Last name updated successfully")
                
            }
            
        }
        
    }
    
 
    func checkPasswordsEquals(newPassword nPassword: String, confirmPassword cPassword: String) -> Bool{
        
        
        if nPassword != "" {

            let nPasswordCheck = checkPasswordFormat(password: nPassword)
            
            if nPasswordCheck {
                //password length is ok, check for confirm pass
                
                if cPassword != "" {
                    
                    let cPasswordCheck = checkPasswordFormat(password: cPassword)
                    
                    if cPasswordCheck {
                        
                        //check is passwords are same
                        
                        if nPassword == cPassword {
                            //password are same
                            
                            return true
                        }
                        else {
                            // passwords are not the same
                            confirmPasswordTxtField.showError(message: "Password and confirm password must be equal!")
                            return false
                        }
                        
                        
                    }
                    else {
                        //confirm password must be longer then 6 chars
                        confirmPasswordTxtField.showError(message: "Confirm password must be longer then 6 characters!")
                        return false
                    }
                    
                    
                }
                else {
                    //confirm password is not inserted
                    confirmPasswordTxtField.showError(message: "Confirm password must be filled!")
                    return false
                    
                }
                
                
            }
            else {
                // nPassword must be logner then 6 chars
                newPasswordTxtField.showError(message: "Password must be longer then 6 characters!")
                return false
            }
            
        }
        else {
            // new password txt field is empty
            newPasswordTxtField.showError(message: "Password must be filled!")
            return false
        }
        
        
        
    }
    
    func checkPasswordFormat(password pass: String) -> Bool{
        
        if pass.count > 6 {
            return true
        }
        else{
          return false
        }
        
    }
    
    func updatePersonsUI(firstName usersFirstName: String,
                         lastName usersLastName:String,
                         mobileNumber usersMobileNumber: String,
                         birthDate usersBirthDate: Date)
                        {
        // Update the UI
        
        if usersFirstName != "" {
            firstNameTxtField.text = usersFirstName
            
        }
        else {
            firstNameTxtField.placeholder = "First Name"
        }
        
        if usersLastName != "" {
            lastNameTxtField.text = usersLastName
        }
        else {
            lastNameTxtField.placeholder = "Last Name"
        }
        
        if usersMobileNumber != "" {
            mobileNumberTxtField.text = usersMobileNumber
        }
        else {
            mobileNumberTxtField.placeholder = "Mobile Number"
        }
        
        //implement some type of checking for the birth date
        
        dateOfBirth.text = usersBirthDate.description
    }
    
    func setupPersonalView(){
        
        if let currentUsersEmail = Auth.auth().currentUser?.email {
            
            
            
            var usersFirstName    = ""
            var usersLastName     = ""
            var usersMobileNumber = ""
            var usersBirthDate    = Date()
           
            
            let db = Firestore.firestore()
            db.collection("users").whereField("personsEmail", isEqualTo: currentUsersEmail).getDocuments { (snapshot, error) in
                
                if error != nil {
                    print("Error while getting info about the current user")
                    print(error!)
                }
                    
                else {
                    
                    for document in (snapshot?.documents)! {
                        
                        self.databaseDocumentID = document.documentID
                        
                        if let firstName = document.data()["personsName"] as? String {
                            usersFirstName = firstName
                            
                        }
                        
                        if let lastName = document.data()["personsLastName"] as? String {
                            usersLastName = lastName
                            
                        }
                        
                        if let mobile = document.data()["mobileNumber"] as? String {
                            usersMobileNumber = mobile
                        }
                        
                        if let birthDate = document.data()["personsBirthDate"] as? Date {
                            usersBirthDate = birthDate
                        }
                        
                    }
        
                }
                
                self.updatePersonsUI(firstName: usersFirstName,
                                lastName: usersLastName,
                                mobileNumber: usersMobileNumber,
                                birthDate: usersBirthDate)
            
              }
            
            
        
            
        }
        
        
    }
    

}



