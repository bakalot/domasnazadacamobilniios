//
//  DetailMenuItemVC.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 1/22/19.
//  Copyright © 2019 Martin Stojcev. All rights reserved.
//

import UIKit

class DetailMenuItemVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    

    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var detailTitle: UILabel!
    @IBOutlet weak var detailDescription: UILabel!
    var detailMenuItem:MenuItem!
    var cartItems = [CartItem]()
    
    @IBOutlet weak var selectCrustLbl: UILabel!
    @IBOutlet weak var selectSizeLbl: UILabel!
    
    @IBOutlet weak var quantity: UITextField!
    
    @IBOutlet weak var addToCartBtn: UIButton!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var priceLbl: UILabel!
    var pickerData: [String] = [String]()
    var isSelectedCrust:Bool = false
    var isSelectedSize: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let crustTap = UITapGestureRecognizer(target: self, action: #selector(showCrustPicket(sender:)))
        let sizeTap = UITapGestureRecognizer(target: self, action: #selector(showSizePicket(sender:)))
        selectCrustLbl.isUserInteractionEnabled = true
        selectSizeLbl.isUserInteractionEnabled = true
        selectCrustLbl.addGestureRecognizer(crustTap)
        selectSizeLbl.addGestureRecognizer(sizeTap)
        
        picker.delegate = self
        picker.dataSource = self
        pickerData = ["auuf","mauuf"]
        if let menuItm = detailMenuItem {
            self.detailImageView.image = menuItm.image ?? UIImage()
            self.detailImageView.contentMode = .scaleAspectFit
            self.detailTitle.text = menuItm.title ?? ""
            self.detailDescription.text = menuItm.description ?? ""
            if menuItm.crust?.count == 0{
                selectSizeLbl.isHidden = true
                selectCrustLbl.isHidden = true
                
                
            }
            
        }
        
    }
   
    
    @objc func showCrustPicket(sender: UIGestureRecognizer){
        picker.isHidden = false
        isSelectedSize = false
        isSelectedCrust = true
        pickerData = detailMenuItem.crust!
        picker.reloadAllComponents()
    }
    
    @objc func showSizePicket(sender: UIGestureRecognizer){
        picker.isHidden = false
        isSelectedSize = true
        isSelectedCrust = false
        pickerData = detailMenuItem.sizes!
        picker.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       
          return pickerData.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if isSelectedCrust{
            selectCrustLbl.text = pickerData[row]
            
        }
        else if isSelectedSize {
            selectSizeLbl.text = pickerData[row]
        }
        picker.isHidden = true
    }
    
    @IBAction func addItemToCart(_ sender: Any) {
        if isSelectedSize && isSelectedCrust {
        if selectSizeLbl.text != "Select size" && selectCrustLbl.text != "Select crust" && quantity.text != "" {
            
            let quantityInt = Int(quantity.text!)
            let cartItem = CartItem(image: detailImageView.image, title: detailMenuItem.title, size: selectSizeLbl.text, crust: selectCrustLbl.text, price: detailMenuItem.price!, quantity: quantityInt)
            let alert = UIAlertController(title: "Great Job ", message: "Your item was successfully added to to cart.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { (okAction) in
             
                 CartManager.sharedInstance.addItem(item: cartItem)
            }
            
            alert.addAction(ok)
            
            present(alert, animated: true, completion: nil)
            
         }
        else{
            let alert = UIAlertController(title: "Error ", message: "Please select crust, size and input the quantity", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            
            present(alert, animated: true, completion: nil)
        }
        }
        else if(quantity.text != ""){
            let alert = UIAlertController(title: "Great Job ", message: "Your item was successfully added to to cart.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { (okAction) in
                
                let quantityInt = Int(self.quantity.text!)
                let cartItem = CartItem(image: self.detailImageView.image, title: self.detailMenuItem.title, size: "", crust: "", price: self.detailMenuItem.price!, quantity: quantityInt)
                CartManager.sharedInstance.addItem(item: cartItem)
            }
            alert.addAction(ok)
            
            present(alert, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Error ", message: "Please input the quantity", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            
            present(alert, animated: true, completion: nil)
        }
    }
    

    @IBAction func calculatePrice(_ sender: Any) {
        let quantityFloat = Float(quantity.text!)
        priceLbl.text = "Price: " + String((quantityFloat!*detailMenuItem.price! * 50).rounded()) + " DEN"
        priceLbl.isHidden = false
    }
    
    @IBAction func shareItem(_ sender: Any) {
        let name = detailTitle.text ?? ""
        let image = detailImageView.image ?? UIImage()
        let text = "This item is shared from MartinsPizza app, open/download to view this item."
        let textContent = name+". "+text
        
        let activityController = UIActivityViewController(activityItems: [textContent], applicationActivities: nil)
        present(activityController, animated: true) {
            let alert = UIAlertController(title: "Sharing success", message: "You shared successfully your item.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
