//
//  PizzaVC.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 12/17/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseUI

class PizzaVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var pizzaTableView: UITableView!
    var menuItems:[MenuItem] = [MenuItem]()
    var imageNames:[String] = [String]()
    let db = Firestore.firestore()
    let storage = Storage.storage()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        db.collection("pizza").getDocuments { (snapshot, error) in
            
            if let err = error {
                print("ERROR: \(err)")
            }
            else{
                for document in snapshot!.documents {
                    //print("\(document.documentID) => \(document.data())")
                    let currentDocData = document.data() as? Dictionary<String,Any> ??  [:]
                    let name = currentDocData["name"] as? String ?? ""
                    let description = currentDocData["description"] as? String ?? ""
                    let crusts = currentDocData["crust"] as? [String]
                    let sizes = currentDocData["sizes"] as? [String]
                    let priceString = currentDocData["price"] ?? "" as? String
                    let price = (priceString as! NSString).floatValue
                    let imageName = name.lowercased().replacingOccurrences(of: " ", with: "-")
                    self.imageNames.append(imageName)
                    let currentItem = MenuItem(image: UIImage(), title: name, description: description, sizes: sizes, crust: crusts,price: price)
                    self.menuItems.append(currentItem)
                    self.pizzaTableView.reloadData()
                    
                }
                
            }
            
        }
       
    }
    

    
    func downloadMenuImages(imageNames: [String]){
        
        let reference = storage.reference()
        for image in imageNames{
            // Create a reference to the file you want to download
            
            let islandRef = reference.child("pizza/\(image).png")
            
            // Create local filesystem URL
            let localURL = URL(string: "\(image).png")!
            
            // Download to the local filesystem
            let downloadTask = islandRef.write(toFile: localURL) { url, error in
                if let error = error {
                    print("error while downloading \(image).png")
                    print(error)
                } else {
                    // Local file URL for "images/island.jpg" is returned
                    print("successfully downloaded \(image).png")
                    let currentCell = self.pizzaTableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? MenuTableViewCell
                    let imgView = currentCell?.cellImageView
                    imgView?.sd_setImage(with: url, completed: { (image, error, chacheType, url) in
                        if let err = error{
                            print("error while setting image")
                        }
                        else{
                            print("successfully set image")
                        }
                    })
                
                }
            }
            
        }
        pizzaTableView.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentMenuItem = menuItems[indexPath.row]
        let cell = pizzaTableView.cellForRow(at: indexPath) as? MenuTableViewCell
        currentMenuItem.image = cell?.cellImageView.image
        
        if let detailMenuVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailMenuItemVC") as? DetailMenuItemVC {
            detailMenuVC.detailMenuItem = currentMenuItem
            present(detailMenuVC, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as? MenuTableViewCell {
            
            let currentMenuItem = menuItems[indexPath.row]
            //guard let currentMenuItemImage = currentMenuItem.image else {return cell}
            guard let currentMenuItemTitle = currentMenuItem.title else {return cell}
            let cellImgName = imageNames[indexPath.row]
            let reference = storage.reference(withPath: "pizza/\(cellImgName).png")
            let imgView = cell.cellImageView
            let placeHolderImage = UIImage()
            imgView?.sd_setImage(with: reference)
            guard let currentMenuItemDescription = currentMenuItem.description else {return cell}

            cell.setPromoCell(title: currentMenuItemTitle, description: currentMenuItemDescription)
            
            
            return cell
        }
        else {
            return UITableViewCell()
        }
        
        
    }
   

}
