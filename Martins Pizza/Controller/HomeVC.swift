//
//  HomeVC.swift
//  Martin's Pizza
//
//  Created by Martin Stojcev on 6/15/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit
import Firebase
import TKSubmitTransition
import UserNotifications


class HomeVC: UIViewController, UIViewControllerTransitioningDelegate {
    
   
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var logoImgView: UIImageView!
    @IBOutlet weak var headerTextLabel: UILabel!
    
    //Home Cards
    @IBOutlet weak var deliveryCard: HomeCard!
    @IBOutlet weak var carryCard: HomeCard!
    @IBOutlet weak var loginCard: HomeCard!
    @IBOutlet weak var menuCard: HomeCard!
    
    @IBOutlet weak var loggedUsersView: UIView!
    @IBOutlet weak var loggedUserStackView: UIStackView!
    @IBOutlet weak var accOrderStackView: UIStackView!
    @IBOutlet weak var menuTrackerStackView: UIStackView!
    @IBOutlet weak var othersFinderStackVIew: UIStackView!
    @IBOutlet weak var logoutStackView: UIStackView!
    
    @IBOutlet weak var myAccountButton: UIButton!
    @IBOutlet weak var myOrderButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var scanBarcodeButton: UIButton!
    
    @IBOutlet weak var othersButton: UIButton!
    @IBOutlet weak var storeFinderButton: UIButton!
    @IBOutlet weak var logoutButton: TKTransitionSubmitButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view.backgroundColor = AppColor.black.rawValue
        
        headerView.backgroundColor = AppColor.black.rawValue
        
        logoImgView.image = UIImage(named: "headerLogo")
        
        headerTextLabel.textColor = AppColor.red.rawValue
        
       setupNotifications()
        
        // Setup delivery card
        
        setupHomeCard(cardView: deliveryCard,
                      imageName: "deliveryImg",
                      separatorColor: .white,
                      mainText: "Delivery",
                      descriptionText: "in less then 30 minutes guaranteed",
                      cardColor: AppColor.green.rawValue
                     )
        // Setup carry card
        
        setupHomeCard(cardView: carryCard,
                      imageName: "carryImg",
                      separatorColor: .white,
                      mainText: "Carry out",
                      descriptionText: "Pick up your order at Martin's store",
                      cardColor: AppColor.blue.rawValue
                     )
        
        // Setup login card
        
        setupHomeCard(cardView: loginCard,
                      imageName: "profileImg",
                      separatorColor: .white,
                      mainText: "Log In",
                      descriptionText: "Log in to reorder your favourites",
                      cardColor: AppColor.brown.rawValue
                     )
        
        
        // Setup Menu card
        
        setupHomeCard(cardView: menuCard,
                      imageName: "menuImg",
                      separatorColor: .white,
                      mainText: "Menu",
                      descriptionText: "View our menu and offers",
                      cardColor: AppColor.red.rawValue
                     )
        
        //Gesture recognizer for each view (with one gesture recognizer only works on the last added view)
        
        let tappedDelivery = UITapGestureRecognizer(target: self, action: #selector(navigateToDelivery))
        deliveryCard.addGestureRecognizer(tappedDelivery)
        
        let tappedCarry = UITapGestureRecognizer(target: self, action: #selector(navigateToCarry))
        
        carryCard.addGestureRecognizer(tappedCarry)
        
        let tappedLogin = UITapGestureRecognizer(target: self, action: #selector(navigateToLogin))
        
        loginCard.addGestureRecognizer(tappedLogin)
        
        let tappedMenu = UITapGestureRecognizer(target: self, action: #selector(navigateToMenu))
        
        menuCard.addGestureRecognizer(tappedMenu)
        
        
      
        
    }
    
    func showLoggedOutViews(){
        
        loggedUsersView.isHidden = true
        loginCard.isHidden       = false
        menuCard.isHidden        = false
        
    }
    
    func showLoggedUserView(){
        // When the user is logged in, show the detailed view on home screen
        
        loggedUsersView.isHidden = false
        
        loggedUsersView.backgroundColor       = AppColor.black.rawValue
        loggedUserStackView.backgroundColor   = AppColor.black.rawValue
        accOrderStackView.backgroundColor     = AppColor.black.rawValue
        menuTrackerStackView.backgroundColor  = AppColor.black.rawValue
        othersFinderStackVIew.backgroundColor = AppColor.black.rawValue
        logoutStackView.backgroundColor       = AppColor.black.rawValue
        
        myAccountButton.backgroundColor       = AppColor.brown.rawValue
        myOrderButton.backgroundColor         = AppColor.brown.rawValue
        menuButton.backgroundColor            = AppColor.brown.rawValue
        scanBarcodeButton.backgroundColor    = AppColor.brown.rawValue
        othersButton.backgroundColor          = AppColor.brown.rawValue
        storeFinderButton.backgroundColor     = AppColor.brown.rawValue
        logoutButton.backgroundColor          = UIColor.white
        
        myAccountButton.tintColor             = UIColor.white
        myOrderButton.tintColor               = UIColor.white
        menuButton.tintColor                  = UIColor.white
        scanBarcodeButton.tintColor          = UIColor.white
        othersButton.tintColor                = UIColor.white
        storeFinderButton.tintColor           = UIColor.white
        logoutButton.tintColor                = AppColor.black.rawValue
        
        logoutButton.showsTouchWhenHighlighted = true
        
        
    }
    
    

    
    @objc func navigateToDelivery(){
        
        if let deliveryVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DeliveryVC") as? DeliveryVC {
            
            present(deliveryVC, animated: true, completion: nil)
        }
        else {
            print("failed to navigate to carry vc")
        }
        
    }
   
    @objc func navigateToCarry(){
        
        if let carryVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CarryVC") as? CarryVC {
            
            present(carryVC, animated: true, completion: nil)
        }
        else {
            print("failed to navigate to carry vc")
        }
        
    }
    
    @objc func navigateToLogin(){
        
        print("login tapped")
        
        if let loginVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC {
            
           present(loginVC, animated: true, completion: nil)
        }
        else {
            print("failed to navigate to login vc")
        }
        
    }
    
    @objc func navigateToMenu(){
        
        if let menuVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MenuTabBarVC") as? MenuTabBarVC {
            print("menu card tapped")
            present(menuVC, animated: true, completion: nil)
        }
        else {
            print("failed to navigate to menu vc")
        }
        
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupHomeCard(cardView cardView: HomeCard, imageName imageName: String, separatorColor separatorColor: UIColor, mainText mainText: String, descriptionText descriptionText: String, cardColor cardColor: UIColor){
        
        cardView.imageView.image = UIImage(named: imageName)
        cardView.separatorView.backgroundColor = separatorColor
        cardView.mainLabel.text = mainText
        cardView.mainLabel.textColor = UIColor.white
        cardView.descriptionLabel.text = descriptionText
        cardView.descriptionLabel.textColor = UIColor.white
        cardView.contentView.backgroundColor = cardColor
        cardView.textView.backgroundColor = cardColor
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Check for logged in user
        
        
        let handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            
            if let loggedUser = user {
                
                let uid = loggedUser.uid
                let email = loggedUser.email
                
                print("Logged user's UID \(uid)")
                print("Logged user's email \(email)")
                
                self.showLoggedUserView()
            }
            else {
                
                print("Error with logging in the user.")
                self.showLoggedOutViews()
            }
        }
    }
    
    //Actions for the detail view buttons
    
    @IBAction func myAccBtnClicked(_ sender: Any) {
        
        if let accountVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AccountVC") as? AccountVC {
            
            
            self.present(accountVC, animated: true)
            
        }
        
    }
    
    @IBAction func myOrderBtnClicked(_ sender: Any) {
        if let shopVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShoppingCartVC") as? ShoppingCartVC {
            self.present(shopVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        navigateToMenu()
    }
    
    
    
    @IBAction func othersBtnClicked(_ sender: Any) {
        
        if let othersVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OthersVC") as? OthersVC {
            
            
            self.present(othersVC, animated: true)
            
        }
        
    }
    
    @IBAction func storeFinderBtnClicked(_ sender: Any) {
        navigateToCarry()
    }
    
    @IBAction func scanBarcodeBtnClicked(_ sender: Any) {
        
        let imagePickerController = UIImagePickerController()
        //imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo source", message: "Choose a source from where to scan the barcode on the item.", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (openCameraAction) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    
    @IBAction func logoutBtnAction(_ sender: Any) {
        
        logoutButton.startLoadingAnimation()
        
        let firebaseAuth = Auth.auth()
        
       
            
            do {
                try firebaseAuth.signOut()
                print("User successfully logged out")
                
                logoutButton.startFinishAnimation(0.5) {
                    
                    
                    // show home vc
                    
                    if let homeVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeVC") as? HomeVC {
                        
                        homeVC.transitioningDelegate = self
                        self.present(homeVC, animated: true)
                        
                    }
                    
                }
                
                self.showLoggedOutViews()
                
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
            
      
        
      
        
    }
    
    func setupNotifications(){
        
        let notificationCenter = UNUserNotificationCenter.current()
        
        notificationCenter.requestAuthorization(options: [.alert, .sound]) { (success, error) in
            if let err = error {
                let alertController = UIAlertController(title: "Notification not enabled", message: "If you want to enable notification for this app go to settings", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: { (okAction) in
                    
                })
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        let notificationContent = UNMutableNotificationContent()
        
        notificationContent.title = "New Deals"
        notificationContent.body = "Check out our new menu, we have great deals for you."
        
        let date = Date().addingTimeInterval(2160000)
        
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: uuidString, content: notificationContent, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let err = error {
                print("Error with notification request")
            }
        }
    }
    
    

}
