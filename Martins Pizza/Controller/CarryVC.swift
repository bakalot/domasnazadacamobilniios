//
//  CarryVC.swift
//  Martin's Pizza
//
//  Created by Martin Stojcev on 6/16/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit
import MapKit

class CarryVC: UIViewController,MKMapViewDelegate {

    @IBOutlet weak var carryMapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        putPinsOnMap()
        
    }
    
    func putPinsOnMap(){
        
        let martinsPizzaKarpos = MKPointAnnotation()
        martinsPizzaKarpos.title = "Martin's Pizza"
        martinsPizzaKarpos.coordinate = CLLocationCoordinate2D(latitude: 42.004155, longitude: 21.406914)
        carryMapView.addAnnotation(martinsPizzaKarpos)
        
        let martinsPizzaAerodrom = MKPointAnnotation()
        martinsPizzaAerodrom.title = "Martin's Pizza"
        martinsPizzaAerodrom.coordinate = CLLocationCoordinate2D(latitude: 41.985406, longitude: 21.464896)
        carryMapView.addAnnotation(martinsPizzaAerodrom)
        
        let martinsPizzaGjorce = MKPointAnnotation()
        martinsPizzaGjorce.title = "Martin's Pizza"
        martinsPizzaGjorce.coordinate = CLLocationCoordinate2D(latitude: 42.010855, longitude: 21.344593)
        carryMapView.addAnnotation(martinsPizzaGjorce)
        

        let martinsPizzaVeles = MKPointAnnotation()
        martinsPizzaVeles.title = "Martin's Pizza"
        martinsPizzaVeles.coordinate = CLLocationCoordinate2D(latitude: 41.716992, longitude: 21.773563)
        carryMapView.addAnnotation(martinsPizzaVeles)
        
        let martinsPizzaKavadarci = MKPointAnnotation()
        martinsPizzaKavadarci.title = "Martin's Pizza"
        martinsPizzaKavadarci.coordinate = CLLocationCoordinate2D(latitude: 41.434280, longitude: 22.010642)
        carryMapView.addAnnotation(martinsPizzaKavadarci)
        
        let martinsPizzaGevgelija = MKPointAnnotation()
        martinsPizzaGevgelija.title = "Martin's Pizza"
        martinsPizzaGevgelija.coordinate = CLLocationCoordinate2D(latitude: 41.143641, longitude: 22.503245)
        carryMapView.addAnnotation(martinsPizzaGevgelija)
        
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
}
