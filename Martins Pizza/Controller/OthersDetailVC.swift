//
//  OthersDetailVC.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 7/21/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit

class OthersDetailVC: UIViewController {

    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContentView: UIView!
    
    @IBOutlet weak var detailLabel: UILabel!
    
    var detailLabelContent: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = AppColor.black.rawValue
        navigationBar.tintColor = UIColor.white
        navigationBar.barTintColor = AppColor.black.rawValue
        backBtn.title = ""
        backBtn.setBackgroundImage(UIImage(named: "backButtonImg"), for: .normal, barMetrics: UIBarMetrics.default)
        
        scrollViewContentView.backgroundColor = AppColor.black.rawValue
        
        if let labelContent = detailLabelContent {
        
            detailLabel.text = labelContent
        
        }
        
        detailLabel.textColor = UIColor.white
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
