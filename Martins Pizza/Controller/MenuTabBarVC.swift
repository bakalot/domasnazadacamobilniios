//
//  MenuTabBarVC.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 12/16/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit

class MenuTabBarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        prepareTabBar()
    
    }
    
    func prepareTabBar() {
    
        let promoVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PromoVC")
        let paketVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaketVC")
        let pizzaVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PizzaVC")
        let sideVC  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SideVC")
        let beverageVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BeverageVC")
        
        let promoTabIcon = UIImage(named: "promo_icon")
        let promoTabItem = UITabBarItem(title: "Promo", image: promoTabIcon, tag: 0)
        promoVC.tabBarItem = promoTabItem
        let packetTabIcon = UIImage(named: "packet_icon")
        let packetTabItem = UITabBarItem(title: "Paket", image: packetTabIcon, tag: 1)
        paketVC.tabBarItem = packetTabItem
        let pizzaTabIcon = UIImage(named: "pizza_icon")
        let pizzaTabItem = UITabBarItem(title: "Pizza", image: pizzaTabIcon, tag: 2)
        pizzaVC.tabBarItem = pizzaTabItem
        let sidesTabIcon = UIImage(named: "side_icon")
        let sidesTabItem = UITabBarItem(title: "Side", image: sidesTabIcon, tag: 3)
        sideVC.tabBarItem = sidesTabItem
        let beveragesTabIcon = UIImage(named: "beverages_icon")
        let beveragesTabItem = UITabBarItem(title: "Beverage", image: beveragesTabIcon, tag: 4)
        beverageVC.tabBarItem = beveragesTabItem
    
        
        //self.tabBar.setItems([promoTabItem,packetTabItem,pizzaTabItem,sidesTabItem,beveragesTabItem], animated: true)
        let vcs = [promoVC, paketVC, pizzaVC, sideVC, beverageVC]
        setViewControllers(vcs, animated: true)
        tabBar.backgroundColor = AppColor.black.rawValue
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
