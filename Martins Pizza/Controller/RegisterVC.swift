//
//  RegisterVC.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 6/18/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit
import Firebase
import DTTextField
import SwiftEntryKit



class RegisterVC: UIViewController, UITextFieldDelegate, UIScrollViewDelegate {
    
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var backBarItem: UIBarButtonItem!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollsContentView: UIView!
    
    
    
    
    
    //Inputs
    @IBOutlet weak var firstNameTxtField: DTTextField!
    @IBOutlet weak var lastNameTxtField: DTTextField!
    @IBOutlet weak var emailTxtField: DTTextField!
    @IBOutlet weak var passwordTxtField: DTTextField!
    @IBOutlet weak var confirmPasswordTxtField: DTTextField!
    @IBOutlet weak var numberTxtField: DTTextField!
    @IBOutlet weak var dateTxtField: UITextField!
    
    
    @IBOutlet weak var registerBtn: UIButton!
    
    //datepicker
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = AppColor.black.rawValue
        navigationBar.tintColor = UIColor.white
        navigationBar.barTintColor = AppColor.black.rawValue
        backBarItem.title = ""
        backBarItem.setBackgroundImage(UIImage(named: "backButtonImg"), for: .normal, barMetrics: UIBarMetrics.default)
        
        titleLbl.textColor = UIColor.white
        
        registerBtn.backgroundColor = AppColor.red.rawValue
        registerBtn.tintColor = UIColor.white
        registerBtn.layer.cornerRadius = 5
        
        scrollsContentView.backgroundColor = AppColor.black.rawValue
        
        showDatePicker()
        //showToolbar()

        //Text fields delegation
        
//        firstNameTxtField.delegate       = self
//        firstNameTxtField.tag            = 0
//        lastNameTxtField.delegate        = self
//        lastNameTxtField.tag             = 1
//        emailTxtField.delegate           = self
//        emailTxtField.tag                = 2
//        passwordTxtField.delegate        = self
//        passwordTxtField.tag             = 3
//        confirmPasswordTxtField.delegate = self
//        confirmPasswordTxtField.tag      = 4
//        numberTxtField.delegate          = self
//        numberTxtField.tag               = 5
//        dateTxtField.delegate            = self
//        dateTxtField.tag                 = 6
 
    }
    
    func showDatePicker(){
        
        //Format date
        datePicker.datePickerMode = .date
        
        //Toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker))
        let spaceBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelBtn = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        
        toolbar.setItems([doneBtn,spaceBtn,cancelBtn], animated: false)
        
        dateTxtField.inputAccessoryView = toolbar
        dateTxtField.inputView = datePicker
        
        
        
    }
    
    
//    func showToolbar(){
//
//        let toolbarKeyboard = UIToolbar()
//        toolbarKeyboard.sizeToFit()
//
//        let spaceBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
//        let cancelBtn = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
//
//        toolbarKeyboard.setItems([spaceBtn,cancelBtn], animated: false)
//
//        firstNameTxtField.inputAccessoryView = toolbarKeyboard
//        lastNameTxtField.inputAccessoryView = toolbarKeyboard
//        emailTxtField.inputAccessoryView = toolbarKeyboard
//        passwordTxtField.inputAccessoryView = toolbarKeyboard
//        confirmPasswordTxtField.inputAccessoryView = toolbarKeyboard
//        numberTxtField.inputAccessoryView = toolbarKeyboard
//
//    }
    
    func showRegisterPopup(){
        
        // Generate top floating entry and set some properties
      
        var attributes = EKAttributes()
        
        let widthConstraint = EKAttributes.PositionConstraints.Edge.ratio(value: 0.9)
        let heightConstraint = EKAttributes.PositionConstraints.Edge.intrinsic
        let entryColor = EKAttributes.BackgroundStyle.color(color: AppColor.green.rawValue)
        let screenBlur = EKAttributes.BackgroundStyle.visualEffect(style: UIBlurEffectStyle.dark)
        
        attributes.border = .value(color: AppColor.black.rawValue, width: 1)
        attributes.roundCorners = .all(radius: 20)
        attributes.displayDuration = .infinity
        attributes.screenInteraction = .dismiss
        attributes.positionConstraints.verticalOffset = 20
        attributes.positionConstraints.size = .init(width: widthConstraint, height: heightConstraint)
        attributes.entryBackground = entryColor
        attributes.screenBackground = screenBlur
        attributes.position = .bottom
        attributes.windowLevel = .alerts
        
        attributes.lifecycleEvents.willDisappear = {
            
            if let homeVC =  UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as? HomeVC{
                
                self.present(homeVC, animated: true, completion: nil)
            }
        }
        
        
        let title = EKProperty.LabelContent(text: "AWESOME!", style: .init(font: UIFont.boldSystemFont(ofSize: 22), color: AppColor.black.rawValue, alignment: NSTextAlignment.center, numberOfLines: 1))
        
        let description = EKProperty.LabelContent(text: "You have successfully signed up. Now you can customize you orders and review the receips.", style: .init(font: UIFont.boldSystemFont(ofSize: 15), color: UIColor.white, alignment: NSTextAlignment.center, numberOfLines: 0))
        let image = EKProperty.ImageContent(image: UIImage(named: "checkmarks")!)
        
        
        
        let btnTitle = EKProperty.LabelContent(text: "OK", style: .init(font: .boldSystemFont(ofSize: 20), color: AppColor.green.rawValue))
        
        let btnContent = EKProperty.ButtonContent(label: btnTitle, backgroundColor: AppColor.black.rawValue, highlightedBackgroundColor: AppColor.green.rawValue )
        
        
        
        let themeImage = EKPopUpMessage.ThemeImage(image: image)
        
        
        let popupMessage = EKPopUpMessage(themeImage: themeImage, title: title, description: description, button: btnContent) {
            
            SwiftEntryKit.dismiss()
            
            if let homeVC =  UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as? HomeVC{
                
                self.present(homeVC, animated: true, completion: nil)
            }
            
        }
        
        let popupMessageView = EKPopUpMessageView(with: popupMessage)
        
        SwiftEntryKit.display(entry: popupMessageView, using: attributes)
        
    }
    
   
    
    @objc func donedatePicker(){
    
    let formatter = DateFormatter()
    formatter.dateFormat = "dd/MM/yyyy"
    dateTxtField.text = formatter.string(from: datePicker.date)
    self.view.endEditing(true)
    scrollView.setContentOffset(CGPoint.zero, animated: true)
    
    }
    
    @objc func cancelDatePicker(){
        
        self.view.endEditing(true)
         scrollView.setContentOffset(CGPoint.zero, animated: true)
    }

//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        
//        //Try to find the next responder
//        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
//            nextField.becomeFirstResponder()
//            
//        } else {
//            //not found, so remove the keyboard
//            
//        }
//        
//        //Do not add a line brake
//        return false
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }

    
    @IBAction func registerNewUser(_ sender: Any) {
        
        //First validate the inserted info
        
        if let insertedEmail = emailTxtField.text {
            
            let emailValidation = isValidEmail(email: insertedEmail)
            
            if emailValidation {
                 //check for password validation
                
                if let password = passwordTxtField.text, (passwordTxtField.text?.count)! > 6 {
                    
                    //check for type-againg-password field
                    if let confirmPassword = confirmPasswordTxtField.text, (confirmPasswordTxtField.text?.count)! > 6 {
                        
                         //check if password is equal with
                        
                        if password == confirmPassword {
                            //all is ok, register the user
                            
                           
                            Auth.auth().createUser(withEmail: insertedEmail, password: password) { (succ, error) in
                                
                                if let err = error {
                                    print("creating user failed")
                                    print(err)
                                }
                                else {
                                    print("successfully created user")
                                    //add user to the database
                                    let name = self.firstNameTxtField.text ?? ""
                                    let lastName = self.lastNameTxtField.text ?? ""
                                    let birthDate = self.datePicker.date
                                    let mobileNumber = self.numberTxtField.text ?? ""
                                    
                                    let newPerson = Person(withEmail: insertedEmail,
                                                           firstName: name,
                                                           lastName: lastName,
                                                           dateOfBirth: birthDate,
                                                           mobileNumber: mobileNumber,
                                                           transactions: [])
                                    
                                     addPersonToDatabase(person: newPerson)
                                    
                                    self.showRegisterPopup()
                                
                                }
                                
                            }
                            
                        } else {
                            // password it's not equal to checkPassword
                            
                           // print("Password and check password must be equals")
                            passwordTxtField.showError(message: "Password and check password must be equals")
                        }
                        
                        
                      } else {
                        //  confirmPassword is not longer then 6 chars or it's empty
                        
                         //print("Confirm password must be longer then 6 chars")
                        if confirmPasswordTxtField.text == ""{
                        
                            confirmPasswordTxtField.showError(message: "Confirm password must be inserted")
                         }
                        else {
                            
                            confirmPasswordTxtField.showError(message: "Confirm password must be longer then 6 chars")
                            
                        }
                        
                            }
                }
                
                else {
                    //password is not longer then 6 chars or it's empty
                        //print("Password must be longer then 6 chars")
                    if passwordTxtField.text == "" {
                        passwordTxtField.showError(message: "Password must be inserted")
                     }
                    else {
                    
                        passwordTxtField.showError(message: "Password must be longer then 6 chars")
                     }
                    
                }
                
            }
                
            else {
                 //email is not in the desired format
                //print("Email is not in the desired format")
                
                if insertedEmail == "" {
                emailTxtField.showError(message:"Email must be inserted!")
                }
                else {
                    emailTxtField.showError(message:"Email is not in the desired format")
                }
               
            }
            
            
        } else {
            
           
           
        }
        
        
        
    }
    
    func showAlert(){
        
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
