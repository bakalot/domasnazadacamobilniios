//
//  OthersVC.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 7/21/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit

class OthersVC: UIViewController {
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    
    @IBOutlet weak var backBarItemBtn: UIBarButtonItem!
    var detailTextToSend: String?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = AppColor.black.rawValue
        //navigationBar.tintColor = UIColor.white
        //navigationBar.barTintColor = AppColor.black.rawValue
        navigationBar.tintColor = UIColor.white
        navigationBar.barTintColor = AppColor.black.rawValue
        backBarItemBtn.title = ""
        backBarItemBtn.setBackgroundImage(UIImage(named: "backButtonImg"), for: .normal, barMetrics: UIBarMetrics.default)
        
        
        
    }
    
    @IBAction func showDetailView(_ sender: Any) {
        
        if let btnPressed = sender as? UIButton {
            print("Button pressed: \((btnPressed.titleLabel?.text!)!)")
            
            let btnPressedTitle = (btnPressed.titleLabel?.text!)!
            
            
            if btnPressedTitle == "TERM OF USE" {
                
                    detailTextToSend = Constants.temrsOfUse
            }
            
            if btnPressedTitle == "PRIVACY POLICY" {
                
                detailTextToSend = Constants.privacyPolicy
            }
            
            if btnPressedTitle == "TERMS & CONDITIONS" {
                
                detailTextToSend = Constants.acceptanceTermsOfUse
            }
            
        }
        

            performSegue(withIdentifier: "segueToOthersDetail", sender: self)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        print("prepareForSegue method called")
        if segue.identifier == "segueToOthersDetail"{

            print("detailTextToSend: \(detailTextToSend)")
            if let destination = segue.destination as? OthersDetailVC {


                if let detailText = detailTextToSend {

                    destination.detailLabelContent = detailText
                }


            }

    }

        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
