//
//  BeverageVC.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 12/17/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseUI

class BeverageVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    

    @IBOutlet weak var beverageTableView: UITableView!
    var menuItems:[MenuItem] = [MenuItem]()
    var imageNames:[String] = [String]()
    let db = Firestore.firestore()
    let storage = Storage.storage()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        db.collection("beverage").getDocuments { (snapshot, error) in
            
            if let err = error {
                print("ERROR: \(err)")
            }
            else{
                for document in snapshot!.documents {
                    //print("\(document.documentID) => \(document.data())")
                    let currentDocData = document.data() as? Dictionary<String,Any> ??  [:]
                    let name = currentDocData["name"] as? String ?? ""
                    let description = currentDocData["description"] as? String ?? ""
                    let priceString = currentDocData["price"] ?? "" as? String
                    let price = (priceString as! NSString).floatValue
                    let imageName = name.lowercased().replacingOccurrences(of: " ", with: "-")
                    self.imageNames.append(imageName)
                    let currentItem = MenuItem(image: UIImage(), title: name, description: description, sizes: [], crust: [],price: price)
                    self.menuItems.append(currentItem)
                    self.beverageTableView.reloadData()
                    
                }
                
            }
            
        }
    }
    

    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentMenuItem = menuItems[indexPath.row]
        let cell = beverageTableView.cellForRow(at: indexPath) as? MenuTableViewCell
        currentMenuItem.image = cell?.cellImageView.image
        
        if let detailMenuVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailMenuItemVC") as? DetailMenuItemVC {
            detailMenuVC.detailMenuItem = currentMenuItem
            present(detailMenuVC, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as? MenuTableViewCell {
            
            let currentMenuItem = menuItems[indexPath.row]
            guard let currentMenuItemImage = currentMenuItem.image else {return cell}
            guard let currentMenuItemTitle = currentMenuItem.title else {return cell}
            guard let currentMenuItemDescription = currentMenuItem.description else {return cell}
            let cellImgName = imageNames[indexPath.row]
            let reference = storage.reference(withPath: "beverage/\(cellImgName).png")
            let imgView = cell.cellImageView
            let placeHolderImage = UIImage()
            imgView?.sd_setImage(with: reference)
            cell.setPromoCell(title: currentMenuItemTitle, description: currentMenuItemDescription)
            return cell
        }
        else {
            return UITableViewCell()
        }
        
    }
   

}
