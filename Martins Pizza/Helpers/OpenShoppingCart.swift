//
//  OpenShoppingCart.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 1/7/19.
//  Copyright © 2019 Martin Stojcev. All rights reserved.
//

import UIKit

extension UIViewController {

    @IBAction func openShoppingCart(){
        if let shoppingCartVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShoppingCartVC")as? ShoppingCartVC {
            
            present(shoppingCartVC, animated: true, completion: nil)
        }
    }


}
