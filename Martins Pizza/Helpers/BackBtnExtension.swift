//
//  BackBtnExtension.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 6/22/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    @IBAction func backPressed(_ sender: Any){
     
        print("dismissing")
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    //        else { // if view controller is navigation pushed
    //            print("popping" )
    //            self.navigationController?.popViewController(animated: true)
    //
    //        }
    
    @IBAction func backToHome(_ sender: Any) {
        
        if let homeVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as? HomeVC{
            present(homeVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func dismissShoppingCart(){
    
     self.dismiss(animated: true, completion: nil)
    
    }
    
    
}
