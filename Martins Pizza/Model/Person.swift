//
//  Person.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 7/10/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import Foundation
import Firebase

struct Person {
    
    var name        : String? = nil
    var lastName    : String? = nil
    let email       : String
    var dateOfBirth : Date?   = nil
    var mobileNumber: String? = nil
    var transactions: [Transaction]? = nil
    
    init(withEmail email:String){
        
        self.email = email
        
    }
    
    init(withEmail email    :String,
         firstName name     :String,
         lastName           : String,
         dateOfBirth        : Date,
         mobileNumber mobile: String,
         transactions: [Transaction]
        ){
        
        self.name           = name
        self.lastName       = lastName
        self.email          = email
        self.dateOfBirth    = dateOfBirth
        self.mobileNumber   = mobile
        self.transactions   = transactions
        
        
    }
    


}

func addPersonToDatabase(person: Person){
    
    let db = Firestore.firestore()
    let settings = db.settings
    settings.areTimestampsInSnapshotsEnabled = true
    db.settings = settings
    
    //print("persons email: \(person.email)")
    
    var dict: [String : Any] = ["personsEmail" : person.email]
    
    if let name = person.name {
        dict["personsName"] = name
    }
    
    if let lastName = person.lastName {
        dict["personsLastName"] = lastName
    }
    if let mobile = person.mobileNumber {
        dict["mobileNumber"] = mobile
    }
    if let birthDate = person.dateOfBirth {
        dict["personsBirthDate"] = birthDate
    }
    
    db.collection("users").document().setData(dict)
    
    
}



    
    




