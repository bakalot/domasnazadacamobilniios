//
//  MenuItem.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 12/17/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit

class MenuItem {
    
    var image: UIImage?
    var title: String?
    var description: String?
    var sizes: [String]?
    var crust: [String]?
    var price: Float?
    
    
    init(image: UIImage?, title: String?, description: String?, sizes: [String]?, crust: [String]?, price: Float?) {
        
        if let img = image {
            self.image = img
        }
        if let titleString = title {
            self.title = titleString
        }
        if let descriptionString = title {
            self.description = description
        }
        if let sizesString = sizes {
            self.sizes = sizesString
        }
        if let crustString = crust {
            self.crust = crustString
        }
        if let priceEntered = price {
            self.price = priceEntered
        }
    }
    
    
    
}

