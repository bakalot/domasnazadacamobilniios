//
//  AppColors.swift
//  Martin's Pizza
//
//  Created by Martin Stojcev on 6/15/18.
//  Copyright © 2018 Martin Stojcev. All rights reserved.
//

import UIKit

enum AppColor{

    case black
    case blue
    case brown
    case green
    case red
    
}

extension AppColor: RawRepresentable {
    
    typealias RawValue = UIColor
    
    init?(rawValue: RawValue) {
        
        switch rawValue {
        case UIColor(red: 0.043, green: 0.020, blue: 0.000, alpha: 1): self = .black
            
        case UIColor(red: 0.314, green: 0.773, blue: 0.898, alpha: 1): self = .blue
            
        case UIColor(red: 0.949, green: 0.690, blue: 0.302, alpha: 1): self = .brown
            
        case UIColor(red: 0.255, green: 0.604, blue: 0.110, alpha: 1): self = .green
            
        case UIColor(red: 0.812, green: 0.090, blue: 0.090, alpha: 1): self = .red
            
        default: return nil
            
        }
        
    }
    
    
    var rawValue: RawValue {
        
        switch self {
           
        case .black: return  UIColor(red: 0.043, green: 0.020, blue: 0.000, alpha: 1)
        case .blue:  return  UIColor(red: 0.314, green: 0.773, blue: 0.898, alpha: 1)
        case .brown: return  UIColor(red: 0.949, green: 0.690, blue: 0.302, alpha: 1)
        case .green: return  UIColor(red: 0.255, green: 0.604, blue: 0.110, alpha: 1)
        case .red:   return  UIColor(red: 0.812, green: 0.090, blue: 0.090, alpha: 1)
            
            
        }
    }
    
    
    
}
