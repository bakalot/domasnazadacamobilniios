//
//  CartItem.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 1/22/19.
//  Copyright © 2019 Martin Stojcev. All rights reserved.
//

import UIKit

class CartItem {
 
    var image: UIImage?
    var title: String?
    var size: String?
    var crust: String?
    var price: Float?
    var quantity: Int?
    
    
    init(image: UIImage?, title: String?, size: String?, crust: String?, price: Float?, quantity: Int?) {
        
        if let img = image {
            self.image = img
        }
        if let titleString = title {
            self.title = titleString
        }
       
        if let sizesString = size {
            self.size = sizesString
        }
        if let crustString = crust {
            self.crust = crustString
        }
        if let priceEntered = price {
            self.price = priceEntered
        }
        if let itemQuantity = quantity {
            self.quantity = itemQuantity
        }
    }
    
}
