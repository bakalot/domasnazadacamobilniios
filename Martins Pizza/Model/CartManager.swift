//
//  CartManager.swift
//  Martins Pizza
//
//  Created by Martin Stojcev on 1/23/19.
//  Copyright © 2019 Martin Stojcev. All rights reserved.
//

import UIKit

class CartManager: NSObject, NSCoding {
    
    static var sharedInstance = CartManager()
    var cartItems: [CartItem]
    
    override init() {
        
        cartItems = []
    }
        
    required convenience init?(coder aDecoder: NSCoder) {
        
        self.init()
    }
    
    func encode(with aCoder: NSCoder) {
        
    }
    
    
     func addItem(item: CartItem){
        CartManager.sharedInstance.cartItems.append(item)
        saveState()
    }
    
     func removeItem(atIndex: Int){
        
       CartManager.sharedInstance.cartItems.remove(at: atIndex)
        saveState()
    }
    
     func clearCart(){
        CartManager.sharedInstance.cartItems = []
        saveState()
    }
    
     func numberOfItemsInCart() -> Int{
        return CartManager.sharedInstance.cartItems.count
    }
    
     func totalPriceInCart() -> Float {
        var totalPrice: Float = 0
        for item in CartManager.sharedInstance.cartItems{
            totalPrice += (item.price! * Float(item.quantity!))
        }
        return totalPrice
    }
    
    func productAtIndexPath(indexPath: IndexPath) -> CartItem {
        return CartManager.sharedInstance.cartItems[indexPath.row]
    }
    
     func saveStateSync()
    {
        let def = UserDefaults()
        let data = NSKeyedArchiver.archivedData(withRootObject: CartManager.sharedInstance)
        def.set(data,forKey:"shoppingCart")
        def.synchronize()
        print("State saved")
    }

     func saveState(){
        DispatchQueue.global(qos: .background).async {
            self.saveStateSync()
        }

    }
    
     func loadState()
    {
        print("loadState called")
        let def = UserDefaults()
        if let data = def.data(forKey:"shoppingCart"){
            guard let retrivedCartItems = NSKeyedUnarchiver.unarchiveObject(with: data) else {return}
            CartManager.sharedInstance = (retrivedCartItems as? CartManager)!
            print("loadState finished")
        }

    }
}
